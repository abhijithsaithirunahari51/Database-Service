package com.abhijithsai.stockservice.databaseservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.abhijithsai.stockservice.databaseservice.model.Quote;

@Service
public interface QuotesRepository extends JpaRepository<Quote,Integer>{

	public List<Quote> findByUserName(String username);
	
}
