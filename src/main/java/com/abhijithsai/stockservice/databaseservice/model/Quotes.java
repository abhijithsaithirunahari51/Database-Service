package com.abhijithsai.stockservice.databaseservice.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Quotes {
	
	private String userName;
	private List<String> quotes;
	
}
